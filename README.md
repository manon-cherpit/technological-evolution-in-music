TECHNOLOGICAL EVOLUTION IN MUSIC

There was a time, when listening to our favorite music whenever we wanted, wasn’t possible, because of the lack of means. Indeed, the idea of producing a music in the purpose to stock it and listen to it later, wasn’t as easy as it is today. But luckily thanks to the technological progress, the music sector was able to evolve remarkably. 

NEW INSTRUMENTS

Over the years and after research, there has been a noticeable evolution in the instrument craft making. For example, the first means of recording music was made possible with vinyl support, imagined by Charles Cros in 1877 and commercialized by Thomas Edison the year after. At the time, this new media allowed families to have access to musical novelties.

Furthermore, traditional acoustic guitars have inspired the creation of electric guitars after technological progress in materials, that made it possible. Electric guitars are the first amplified instruments, invented by George Beauchamp and Adolph Rickenbacher. The amplification of the guitar allowed guitarists to perform as solo artists in orchestra which was normally reserved for piano or brass instruments. 

During the sixties, a new form of loud amplification for instruments boosted the sales of instruments manufacturers. Therefore, Technical revolution in music allowed many artists to experience and create new sounds, such as “distortion effect” made famous by Jimi Hendrix. These new sounds effects allowed rock music to evolve in different direction (psychedelic, hard and progressive). These new technologies in musical research gave birth to historical rock festivals like Woodstock and Isle of Wight-England. In 1964, the invention of the audio cassette allowed people to record the music they chose. 

At the end of the seventies, Sony commercializes the “Walkman” allowing people to listen to audio-cassettes. Immediately, the success is worldwide because this machine gives people the opportunity to listen to their music daily and contributes to the development of the musical industry. 

Then, the first Atari computers permits the creation of electronic music. It is the new era of computerized music, which brings techno music. Consequently, the development of technology in the music industry allows for the evolution of new instruments and various tools to synthesize sounds. Over the years this research led to the development of new musical styles.  

In 1985, the compact disc is invented, contributing to the renaissance of the music industry by changing from analogical to numerical media and because of its practical size, making it easier to transport and store. In the late nineties, the popularization of informatic, internet and MP3 format, allows music to be compressed on smaller media supports. Thus, the computer itself, has become a music instrument, a recording studio and a listening support. 

SOUND QUALITY AND VARIETY 

Aside from having new instruments, music lovers or sound engineers, benefit from software computer programs, which gives much more sophisticated and higher music quality than before. 

Furthermore, a large choice of sounds can now be exploited, through all the new existing effects, which gives way to new musical styles, such as electro, house, techno…   

INTERNET PIRATING

Nowadays, the downside of easy internet access allows such fraudulent activities on the web as illegal copyrights, practiced by hackers when an artist puts out a new album on the market, avoiding them to get any retribution. 
Consequently, the development of computer technology and specific software programs in the music industry, allows the evolution of new instruments and various tools to synthesize sounds. Over the years these researches led to the development and evolution of new musical styles. However, the music industry is constantly threatened by illegal music downloads and computer hacking.